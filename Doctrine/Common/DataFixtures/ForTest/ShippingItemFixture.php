<?php

namespace Eccube\Doctrine\Common\DataFixtures\ForTest;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Connection;
use Eccube\Entity\ShipmentItem;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;

class ShippingItemFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $app = \Eccube\Application::getInstance();
        $app->initialize();

        /*$shipment_Item = new ShipmentItem();

        $shipment_Item->setProductName($app['faker']->text);
        $shipment_Item->setProductCode($app['faker']->text);
        $shipment_Item->setClassName1($app['faker']->text);
        $shipment_Item->setClassName2($app['faker']->text);
        $shipment_Item->setPrice($app['faker']->numberBetween(0, 10000000));
        $shipment_Item->setQuantity($app['faker']->numberBetween(0, 1000));
        $shipment_Item->setOrder($manager->merge($this->getReference('order1')));
        $Product = $app['orm.em']->getRepository('Eccube\Entity\Product')->findOneBy([
                'id' => $app['faker']->numberBetween(1, 11)//既存テストデータ
            ]);
        $shipment_Item->setProduct($Product);

        $ProductClass = $app['orm.em']->getRepository('Eccube\Entity\ProductClass')->findOneBy([
                'id' => 1//既存テストデータ
            ]);
        $shipment_Item->setProductClass($ProductClass);

        $manager->persist($shipment_Item);
        $manager->flush();*/

        /*ShipmentItemのEntityの作りが特殊で、DataFixtureが動かないので、SQLで投入*/
        
        $Connection = $manager->getConnection();
        $Connection->beginTransaction();

        $params = [
            'product_name' => str_replace('"', '', $app['faker']->title),
            'product_code' => str_replace('"', '', $app['faker']->isbn10),
            'class_name1' => str_replace('"', '', $app['faker']->text),
            'class_name2' => str_replace('"', '', $app['faker']->text),
            'price' => $app['faker']->numberBetween(0, 10000000),
            'quantity' => $app['faker']->numberBetween(0, 10000),
            'shipping_id' => $manager->merge($this->getReference('shipping1'))->getId(),
            'order_id' => $manager->merge($this->getReference('order1'))->getId(),
            'product_id' => $app['faker']->numberBetween(1, 11),
            'product_class_id' => $app['faker']->numberBetween(1, 10)
        ];
        $sql = $this->getSql('dtb_shipment_item', $params);
        $prepare = $Connection->prepare($sql);
        $result = $prepare->execute();
        $Connection->commit();

    }

    public function getOrder()
    {
        return 5; // the order in which fixtures will be loaded
    }

    /**
     * INSERT を生成する.
     *
     * @param string $table_name テーブル名
     * @param array $params カラム名と値の連想配列
     * @return string INSERT 文
     */
    public function getSql($table_name, array $params)
    {
        return 'INSERT INTO '.$table_name.' ('.implode(', ', array_keys($params)).') VALUES ("'.implode('" , "', array_values($params)).'")';
    }
}
