<?php

namespace Eccube\Doctrine\Common\DataFixtures\ForTest;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Connection;
use Eccube\Entity\Order;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;

class OrderFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $app = \Eccube\Application::getInstance();
        $app->initialize();

        $order = new Order();
        $order->setShop($manager->merge($this->getReference('base_info1')));
        $Customer = $app['orm.em']->getRepository('Eccube\Entity\Customer')->findOneBy([
                'id' => 1 //既存テストデータ

            ]);
        $order->setCustomer($Customer);
        $Pref = $app['orm.em']->getRepository('Eccube\Entity\Master\Pref')->findOneBy([
                'id' => $app['faker']->numberBetween(1, 47)
            ]);
        $order->setPref($Pref);
        $Country = $app['orm.em']->getRepository('Eccube\Entity\Master\Country')->findOneBy([
                'id' => 392 // 日本
            ]); 
        $order->setCountry($Country);//日本
        $Sex = $app['orm.em']->getRepository('Eccube\Entity\Master\Sex')->findOneBy([
                'id' => $app['faker']->numberBetween(1, 2)
            ]);
        $order->setSex($Sex);
        $Job = $app['orm.em']->getRepository('Eccube\Entity\Master\Job')->findOneBy([
                'id' => $app['faker']->numberBetween(1, 18)
            ]);
        $order->setJob($Job);
        $Payment = $app['orm.em']->getRepository('Eccube\Entity\Payment')->findOneBy([
                'id' => $app['faker']->numberBetween(1, 4)
            ]);
        $order->setPayment($Payment);
        $DeviceType = $app['orm.em']->getRepository('Eccube\Entity\Master\DeviceType')->findOneBy([
                'id' => $app['faker']->randomElement([1, 2, 10, 99])
            ]);

        $order->setDeviceType($DeviceType);
        //$order->setPreOrder();
        $order->setMessage($app['faker']->text);
        $order->setName01($app['faker']->lastName);
        $order->setName02($app['faker']->firstName);
        $order->setKana01($app['faker']->lastName."カナ");
        $order->setKana02($app['faker']->firstName."カナ");
        //$order->setCompanyName();
        $order->setEmail($app['faker']->email);
        $order->setTel01(explode("-", $app['faker']->phoneNumber)[0]);
        $order->setTel02(explode("-", $app['faker']->phoneNumber)[1]);
        $order->setTel03(explode("-", $app['faker']->phoneNumber)[2]);
        //$order->setFax01();
        //$order->setFax02();
        //$order->setFax03();
        $order->setZip01($app['faker']->postcode."1");
        $order->setZip02($app['faker']->postcode."2");
        $order->setZipcode($app['faker']->postcode."3");
        $order->setAddr01($app['faker']->streetAddress);
        $order->setAddr02($app['faker']->buildingNumber);
        //$order->setBirth();
        $order->setSubtotal($app['faker']->numberBetween(0, 1000000));
        //$order->setDiscount();
        $order->setPointUse($app['faker']->numberBetween(0, 1000));
        $order->setPointAdd($app['faker']->numberBetween(0, 100));
        //$order->setDeliveryFeeTotal();
        //$order->setCharge();
        //$order->setTax();
        $order->setTotal($app['faker']->numberBetween(0, 1000000));
        //$order->setPaymentTotal();
        $order->setPaymentMethod($app['faker']->text);
        $order->setNote($app['faker']->text);
        //$order->setCreateDate();
        $order->setOrderDate($app['faker']->dateTimeBetween($startDate = '-10 days', $endDate = '-1 day'));
        //$order->setCommitDate();
        $order->setPaymentDate($app['faker']->dateTimeBetween($startDate = '-10 days', $endDate = '-1 day'));
        $order->setDelFlg(0);
        //$order->setStaffFlg();
        $OrderStatus = $app['orm.em']->getRepository('Eccube\Entity\Master\OrderStatus')->findOneBy([
                'id' => $app['faker']->randomElement([1, 8])
            ]);

        $order->setOrderStatus($OrderStatus);
        $order->setRealEstateNo($app['faker']->buildingNumber);


        $manager->persist($order);
        $manager->flush();
        $this->addReference('order1', $order);
    }

    public function getOrder()
    {
        return 3; // the order in which fixtures will be loaded
    }
}
