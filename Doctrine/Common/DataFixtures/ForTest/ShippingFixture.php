<?php

namespace Eccube\Doctrine\Common\DataFixtures\ForTest;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Connection;
use Eccube\Entity\Shipping;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;

class ShippingFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $app = \Eccube\Application::getInstance();
        $app->initialize();

        $shipping = new Shipping();

        $Delivery = $app['orm.em']->getRepository('Eccube\Entity\Delivery')->findOneBy([
                'id' => $app['faker']->numberBetween(1, 2)//既存テストデータ
            ]);
        $shipping->setDelivery($Delivery);
        $Pref = $app['orm.em']->getRepository('Eccube\Entity\Master\Pref')->findOneBy([
                'id' => $app['faker']->numberBetween(1, 47)
            ]);
        $shipping->setPref($Pref);

        $Country = $app['orm.em']->getRepository('Eccube\Entity\Master\Country')->findOneBy([
                'id' => 392 // 日本
            ]);
        $shipping->setCountry($Country);

        $DeliveryTime = $app['orm.em']->getRepository('Eccube\Entity\DeliveryTime')->findOneBy([
                'id' => $app['faker']->numberBetween(1, 2)//既存テストデータ
            ]);
        $shipping->setShippingDeliveryTime($DeliveryTime->getDeliveryTime());
        $shipping->setOrder($manager->merge($this->getReference('order1')));

        /*$DeliveryFee = $app['orm.em']->getRepository('Eccube\Entity\DeliveryFee')->findOneBy([
                'id' => $app['faker']->numberBetween(1, 94)//既存テストデータ
            ]);
        $shipping->setDeliveryFee($DeliveryFee);*/
        $shipping->setShippingDeliveryFee($app['faker']->numberBetween(0, 1000));

        $shipping->setName01($app['faker']->lastName);
        $shipping->setName02($app['faker']->firstName);
        $shipping->setKana01($app['faker']->lastName."カナ");
        $shipping->setKana02($app['faker']->firstName."カナ");
        $shipping->setTel01(explode("-", $app['faker']->phoneNumber)[0]);
        $shipping->setTel02(explode("-", $app['faker']->phoneNumber)[1]);
        $shipping->setTel03(explode("-", $app['faker']->phoneNumber)[2]);
        $shipping->setZip01($app['faker']->postcode."1");
        $shipping->setZip02($app['faker']->postcode."2");
        $shipping->setZipcode($app['faker']->postcode."3");
        $shipping->setAddr01($app['faker']->streetAddress);
        $shipping->setAddr02($app['faker']->buildingNumber);
        $shipping->setShippingDeliveryDate($app['faker']->dateTimeBetween($startDate = '-10 days', $endDate = '-1 day'));
        $shipping->setShippingCommitDate($app['faker']->dateTimeBetween($startDate = '-10 days', $endDate = '-1 day'));
        $shipping->setCreateDate($app['faker']->dateTimeBetween($startDate = '-10 days', $endDate = '-1 day'));
        $shipping->setUpdateDate($app['faker']->dateTimeBetween($startDate = '-10 days', $endDate = '-1 day'));
        $shipping->setDelFlg(0);

        $manager->persist($shipping);
        $manager->flush();
        $this->addReference('shipping1', $shipping);

    }

    public function getOrder()
    {
        return 4; // the order in which fixtures will be loaded
    }
}
