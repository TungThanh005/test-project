<?php

namespace Eccube\Doctrine\Common\CsvDataFixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Connection;

/**
 * CSVファイルからデータを取り込むフィクスチャ。CSVと登録先テーブルが全列対応しているマスターデータ用
 * CsvFixtureと機能的に変わらない、ただ前後処理がついている
 *
 * @see https://github.com/doctrine/data-fixtures/blob/master/lib/Doctrine/Common/DataFixtures/FixtureInterface.php
 */
class CsvCopyToFixture implements FixtureInterface
{
    /** @var \SplFileObject $file */
    protected $file;

    protected $entity;

    protected $table_logical;

    protected $table_physical;

    protected $cols;

    protected $header_read_flg;

    protected $last_insert_entities;

    public function __construct(\SplFileObject $file, array $csv)
    {
        $this->file = $file;
        $this->entity = $csv['entity'];
        $this->table_logical = $csv['table_logical'];
        $this->table_physical = $csv['table_physical'];
        $this->cols = $csv['cols'];
        $this->last_insert_entities = $csv['last_insert_entities'];
    }

    public function setHeaderReadFlg($headerReadFlg)
    {
        $this->header_read_flg = $headerReadFlg;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {

        // CSV Reader に設定
        $this->file->setFlags(\SplFileObject::READ_CSV | \SplFileObject::READ_AHEAD | \SplFileObject::SKIP_EMPTY | \SplFileObject::DROP_NEW_LINE);
        
        if (empty($this->file)) {
            throw new \Exception('CSVファイルが指定されていません');
        }
        
        if (empty($this->entity)) {
            throw new \Exception('保存先エンティティが指定されていません');
        }

        if (empty($this->table_physical)) {
            throw new \Exception('削除するテーブル名が指定されていません');
        }


        // ヘッダから保存先を取得する場合は、ヘッダ行をスキップ
        if ($this->header_read_flg) {
            $this->cols = $this->file->current();
            $this->file->next();
        } else {
            if (empty($this->cols)) {
                throw new \Exception('保存元カラムが指定されていません');
            }
        }

        // NO_AUTO_VALUE_ON_ZEROを設定
        //$Connection->exec("SET SESSION sql_mode='NO_AUTO_VALUE_ON_ZERO';");

        if (!class_exists($this->entity)) {
            throw new \Exception("指定のエンティティクラスが存在しません：{$this->entity}");
        }
        $Connection = $manager->getConnection();
        $Connection->beginTransaction();

        //データ削除
        try {
            $sql = "set foreign_key_checks = 0;";
            $Connection->executeQuery($sql);
            $sql = "DELETE FROM `{$this->escapeIdent($this->table_physical)}`;";
            $Connection->executeQuery($sql);
            $sql = "set foreign_key_checks = 1;";
            $Connection->executeQuery($sql);
        } catch (\Exception $e) {
            $Connection->rollback();
            throw new \Exception("{$this->table_logical}の削除でエラーが発生しました。\n{$e->getMessage()}\n{$e->getTraceAsString()}");
        }
        
        //データ登録
        try {
            $row_index = 0;
            while ($fcols = $this->file->current()) {
                $entity = new $this->entity();
                $row_index++;
                foreach ($fcols as $index => $fcol) {
                    $setter = "set" . $this->camelize($this->cols[$index]);
                    $entity->$setter($this->convertUTF8($fcol));
                }
                $manager->persist($entity);
                
                //1000行ごとにフラッシュする
                if ($row_index % 1000 === 0) {
                    $manager->flush();
                    $manager->clear();//もし複数テーブルを操作する場合はエンティティクラス名指定が必要
                }
                $this->file->next();
            }
            //まだflushしていないデータを処理する
            if ($row_index % 1000 !== 0) {
                $manager->flush();
            }
            //最後に挿入するデータ群があれば処理する
            if ($this->last_insert_entities) {
                foreach ($this->last_insert_entities as $last_insert_entity) {
                    $entity = new $this->entity();
                    foreach ($last_insert_entity as $k => $v) {
                        $setter = "set" . $this->camelize($k);
                        $entity->$setter($v);
                    }
                    $manager->persist($entity);
                }
                $manager->flush();
            }
        } catch (\Exception $e) {
            $Connection->rollback();
            $manager->clear();
            throw new \Exception("{$this->table_logical}の{$row_index}件目の登録でエラーが発生しました。\n{$e->getMessage()}\n{$e->getTraceAsString()}");
        }
        $Connection->commit();
    }

    private function convertUTF8(string $row_str)
    {
        return mb_convert_encoding($row_str, 'UTF-8', 'SJIS-win');
    }

    private function camelize(string $s)
    {
        return str_replace([' ', '-', '_'], '', ucwords($s, ' -_'));
    }

    private function escapeIdent($table)
    {
        return preg_replace('/`/', '``', $table);
    }
}
